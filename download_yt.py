from pytube import Channel, YouTube, Playlist
from pytube.cli import on_progress
from pydub import AudioSegment
import argparse
import os


def download_video(video, audio, path_to_save="", num=""):
    if path_to_save != "":
        path_to_save = path_to_save + "/"
    if audio:
        video = YouTube(video, on_progress_callback=on_progress).streams.filter(only_audio=True)
        video = video[len(video) - 1]
    else:
        video = YouTube(video, on_progress_callback=on_progress).streams.get_highest_resolution()
    print(video.default_filename)
    video.download(path_to_save)
    if audio:
        song = AudioSegment.from_file(path_to_save + video.default_filename, 'webm')
        song.export(path_to_save + num + video.default_filename[:len(video.default_filename) - 5] + ".mp3", format="mp3", bitrate='320k')
        os.remove(path_to_save + video.default_filename)

    print()


def download_channel(channel, audio):
    channel = Channel(channel)

    path_to_save = channel.channel_name

    if not os.path.exists(path_to_save):
        os.makedirs(path_to_save)

    for i, video in enumerate(channel):
        print(str(i + 1) + '/' + str(len(channel)))
        download_video(video, audio, path_to_save)


def download_playlist(playlist, audio):
    playlist = Playlist(playlist)

    path_to_save = playlist.title
    print(playlist.title)

    if not os.path.exists(path_to_save):
        os.makedirs(path_to_save)

    for i, video in enumerate(playlist):
        print(str(i + 1) + '/' + str(len(playlist)))
        download_video(video, audio, path_to_save, str(i + 1) + ". ")


parser = argparse.ArgumentParser(description='Download videos, playlists and channels')
parser.add_argument('-c', '--channel', type=str, help='Link to channel')
parser.add_argument('-p', '--playlist', type=str, help='Link to playlist')
parser.add_argument('-v', '--video', type=str, help='Link to video')
parser.add_argument('-a', '--audio', action='store_true', help='Only audio')
args = parser.parse_args()


if args.channel:
    download_channel(args.channel, args.audio)
elif args.video:
    download_video(args.video, args.audio)
elif args.playlist:
    download_playlist(args.playlist, args.audio)
